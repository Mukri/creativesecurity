# CreativeSecurity #

#### What is CS? ####
CS is a Creative server protector! It will protect you from spawning abuse, tnt abuse and much more. It is a free plugins & open source.

#### What does CS provide? ####
* Disable Tnt/Creeper/Other explosions
* Disable Dispensing Items such as Lava/Tnt/MonsterEggs/Water
* Disable Certain Block Placing such as Tnt/CommandBlocks/Lava/Water
* Disable Spawning Certain Mobs such as Wither/Snowman/IronGolem/Dragon/ArmorStand
* Disable Weather Changing

#### Where can I download this plugins? ####
You can get them [HERE](https://www.spigotmc.org/resources/creativesecurity.23574/)

#### Commands & Permissions ####
* /Cs reload - cs.admin

#### Minecraft Version? ####
Should be working for a 1.9+ Minecraft Spigot/Bukkit Server.