package com.mukri.creativesecure.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mukri.creativesecure.Main;


/**
 * CopyRighted by none and Made by DoomGary / Mukri
 * Made on: 5:21:46 PM 
 */

public class Cs implements CommandExecutor {


	public Main plugin;

	public Cs(Main plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String label, String[] args) {

		if(!(sender instanceof Player)) {
			sender.sendMessage("§cYou are not allowed to do this.");
			return true;
		}

		Player p = (Player) sender;

		if(cmd.getName().equalsIgnoreCase("cs")) {
			if(p.hasPermission("cs.admin")) {
				if(args.length == 0) {
					p.sendMessage("§a/Cs Reload §7- Reload config.yml");
				}
				else if(args[0].equalsIgnoreCase("reload")) {
					plugin.reloadConfig();
					p.sendMessage("§7config.yml reloaded.");
				}
			}
		}

		return false;
	}

}
