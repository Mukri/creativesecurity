package com.mukri.creativesecure.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowman;
import org.bukkit.entity.Wither;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;

import com.mukri.creativesecure.Main;

/**
 * CopyRighted by none and Made by DoomGary / Mukri 
 * Made on: 4:55:40 PM
 */

public class WorldsSecurity implements Listener {

	public Main plugin;

	public WorldsSecurity(Main plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onTntExplode(ExplosionPrimeEvent e) {
		if (plugin.getConfig().getBoolean("Disable-Tnt")) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onDispensarCancel(BlockDispenseEvent e) {
		ItemStack item = e.getItem();

		if (item.getType() == Material.WATER_BUCKET) {
			if (plugin.getConfig().getBoolean("Dispensar.Water")) {
				e.setCancelled(true);
			}
		}
		if (item.getType() == Material.LAVA_BUCKET) {
			if (plugin.getConfig().getBoolean("Dispensar.Lava")) {
				e.setCancelled(true);
			}
		}
		if (item.getType() == Material.MONSTER_EGG) {
			if (plugin.getConfig().getBoolean("Dispensar.Monster-Eggs")) {
				e.setCancelled(true);
			}
		}
		if (item.getType() == Material.MONSTER_EGGS) {
			if (plugin.getConfig().getBoolean("Dispensar.Monster-Eggs")) {
				e.setCancelled(true);
			}
		}
		if (item.getType() == Material.TNT) {
			if (plugin.getConfig().getBoolean("Dispensar.Tnt")) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onPlaceItems(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		Block block = e.getBlock();
		String msg = plugin.getConfig().getString("Block-Placing-Msg")
				.replaceAll("&", "§");

		if (block.getType() == Material.TNT) {
			if (plugin.getConfig().getBoolean("Block-Placing.Tnt")) {
				e.setCancelled(true);
				p.sendMessage(msg);
			}
		}
		if (block.getType() == Material.COMMAND
				|| block.getType() == Material.COMMAND_CHAIN
				|| block.getType() == Material.COMMAND_REPEATING
				|| block.getType() == Material.COMMAND_MINECART) {
			if (plugin.getConfig().getBoolean("Block-Placing.Command-Blocks")) {
				e.setCancelled(true);
				p.sendMessage(msg);
			}
		}
	}
	
	@EventHandler
	public void onEntitySpawn(EntitySpawnEvent e) {	
		if(e.getEntity() instanceof ArmorStand) {
			if(plugin.getConfig().getBoolean("Spawning.Armor-Stand")) {
				e.setCancelled(true);
			}
		}
		if(e.getEntity() instanceof Wither) {
			if(plugin.getConfig().getBoolean("Spawning.Wither")) {
				e.setCancelled(true);
			}
		}
		if(e.getEntity() instanceof Snowman) {
			if(plugin.getConfig().getBoolean("Spawning.Snowman")) {
				e.setCancelled(true);
			}
		}
		if(e.getEntity() instanceof IronGolem) {
			if(plugin.getConfig().getBoolean("Spawning.IronGolem")) {
				e.setCancelled(true);
			}
		}
		if(e.getEntity() instanceof EnderDragon) {
			if(plugin.getConfig().getBoolean("Spawning.Dragon")) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onEmptyBucket(PlayerBucketEmptyEvent e) {
		Player p = e.getPlayer();
		String msg = plugin.getConfig().getString("Block-Placing-Msg")
				.replaceAll("&", "§");
		
		if(e.getBucket() == Material.LAVA_BUCKET) {
			if(plugin.getConfig().getBoolean("Block-Placing.Lava")) {
				e.setCancelled(true);
				p.sendMessage(msg);
			}
		}
		if(e.getBucket() == Material.WATER_BUCKET) {
			if(plugin.getConfig().getBoolean("Block-Placing.Water")) {
				e.setCancelled(true);
				p.sendMessage(msg);
			}
		}
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent e) {
		if(plugin.getConfig().getBoolean("Disable-Rain")) {
			e.setCancelled(true);
		}
	}

}
