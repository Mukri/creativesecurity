package com.mukri.creativesecure;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.mukri.creativesecure.commands.Cs;
import com.mukri.creativesecure.listeners.WorldsSecurity;


/**
 * CopyRighted by none and Made by DoomGary / Mukri
 * Made on: 4:35:33 PM 
 */

public class Main extends JavaPlugin {
	
	File cfgFile;
	FileConfiguration cfgConfig;
		
	public void onEnable() {
		listen();
		commands();		
		saveDefaultConfig();
	}
	
	public void listen() {
		getServer().getPluginManager().registerEvents(new WorldsSecurity(this), this);
	}
	
	public void commands() {
		getCommand("cs").setExecutor(new Cs(this));
	}
}
